package fr.octanium.unplaceableblocks;

import org.bukkit.plugin.java.JavaPlugin;

import fr.octanium.unplaceableblocks.commands.Commands;
import fr.octanium.unplaceableblocks.commands.Completion;
import fr.octanium.unplaceableblocks.listeners.Listeners;

public class Main extends JavaPlugin {
	
	@Override
	public void onEnable() {
		saveDefaultConfig();		
		reloadConfig();

		this.getCommand("unplaceable").setExecutor(new Commands(this));
		this.getCommand("unplaceable").setTabCompleter(new Completion());
		
		getServer().getPluginManager().registerEvents(new Listeners(this), this);
		
		getServer().getConsoleSender().sendMessage("[§6UnplaceableBlocks§f] §aOn");
	}
	
	@Override
	public void onDisable() {
		getServer().getConsoleSender().sendMessage("[§6UnplaceableBlocks§f] §cOff");		
	}

}

