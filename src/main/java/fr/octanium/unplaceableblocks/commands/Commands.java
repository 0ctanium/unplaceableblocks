package fr.octanium.unplaceableblocks.commands;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.octanium.unplaceableblocks.Main;
import fr.octanium.unplaceableblocks.util.ItemUtil;

public class Commands implements CommandExecutor {

	private Main main = null;
	
	public Commands(Main main) {
		this.main = main;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length > 0) {
			if(args[0].equalsIgnoreCase("reload")) {
				if(sender.hasPermission("unplaceable.admin")) {
					main.reloadConfig();
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("config-reloaded")));
					return true;
				} else {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("no-permission")));
				}
			}
			return false;
		} else if(sender instanceof Player) {
			if(sender.hasPermission("unplaceable.admin")) {
				Player player = (Player) sender;
				ItemStack item = player.getInventory().getItemInMainHand();
				if(item == null || item.getType() == Material.AIR || item.getAmount() == 0) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("may-hold-item")));
					return true;
				}
				
				String base64 = ItemUtil.toBase64(item);
				List<String> unplacables = main.getConfig().getStringList("unplaceables");
				if(unplacables.contains(base64)) {
					unplacables.remove(base64);
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("changed-to-placeable")));
				} else {
					unplacables.add(base64);
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("changed-to-unplaceable")));
				}

				main.getConfig().set("unplaceables", unplacables);
				main.saveConfig();
				main.reloadConfig();
			} else {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("no-permission")));
			}
		} else {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("not-console")));
		}
		return true;
	}
	
	
}
