package fr.octanium.unplaceableblocks.listeners;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import fr.octanium.unplaceableblocks.Main;
import fr.octanium.unplaceableblocks.util.ItemUtil;

public class Listeners implements Listener {

	private Main main = null;
	
	public Listeners(Main main) {
		this.main = main;
	}
	
	@EventHandler
	public void onBlockPlace(PlayerInteractEvent event) {
		if(!event.getPlayer().hasPermission("unplaceable.bypass")) {
			if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				String base64 = ItemUtil.toBase64(event.getPlayer().getInventory().getItemInMainHand());
				if(isPlacable(base64)) {
					event.setCancelled(true);
					String message = main.getConfig().getString("place-unplaceable-message");
					if(message.equalsIgnoreCase("none") || message == null || message == "") return;
					event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
				}
			}
		}
	}
	
	private boolean isPlacable(ItemStack stack) {
		List<String> unplacables = main.getConfig().getStringList("unplaceables");
		
		return unplacables.stream().filter(base64 -> ItemUtil.fromBase64(base64).isSimilar(stack)).findFirst().orElse(null) != null;
	}
	
	private boolean isPlacable(String base64) {
		return isPlacable(ItemUtil.fromBase64(base64));
	}
	
}
