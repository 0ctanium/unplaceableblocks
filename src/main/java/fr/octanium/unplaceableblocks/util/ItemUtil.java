package fr.octanium.unplaceableblocks.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

public class ItemUtil {

	public static String toBase64(ItemStack item) {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
			
			dataOutput.writeObject(item);
			dataOutput.close();
			
			return Base64Coder.encodeLines(outputStream.toByteArray());
		} catch (Exception e) {
			throw new IllegalStateException("Unable to save item stack.", e);
		}
	}
	
	public static ItemStack fromBase64(String data) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
        BukkitObjectInputStream dataInput = null;
        ItemStack stack = null;
		
        try {        	
            dataInput = new BukkitObjectInputStream(inputStream);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try {
            stack = (ItemStack) (ItemStack) dataInput.readObject();
        } catch (IOException | ClassNotFoundException e) {
            try { dataInput.close(); } catch (IOException e1) {}
            return null;
        }

        try { dataInput.close(); } catch (IOException e1) {}

        return stack;
    }
}
